const path = require('path');
const merge = require('webpack-merge');
const BaseConfig = require('./__webpack/base.config');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanPlugin = require('clean-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const webpack = require('webpack');

const ENTRY = './src/browser.jsx';

///////////// DEV CONFIG /////////////////
const baseDevCfg = BaseConfig();
const getDevConfig = () => {
  const outdir = path.resolve(__dirname, 'build','dev');
  const StylelintPlugin = require('stylelint-webpack-plugin');
  const devConfig = merge([
    {
      resolve:{
        //lazy-load nggak jalan kalo 
        //nggak pake ini
        extensions:['.js','.jsx']
      }
    },
    {
      entry: ENTRY,
      output: {
        path: outdir,
        filename:'./js/bundle.js',
        chunkFilename:'./js/[name].chunk.js'
      }
    },
    baseDevCfg.devServer({
      contentBase: outdir
    }),
    baseDevCfg.modules(null,[
      {
        enforce: 'pre',
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'eslint-loader'
      }
    ]),
    baseDevCfg.plugins([
      new webpack.LoaderOptionsPlugin({
        options: {
          eslint: {
            formatter: require('eslint-formatter-pretty')
          }
        }
      }),
      new StylelintPlugin({
        configFile: '.stylelintrc.json',
        syntax: 'scss'
      }),
      new HtmlWebpackPlugin({
        title: 'DEV',
        template: './__webpack/__template.html',
        filename:'index.html'
      }),
      new CleanPlugin([ outdir ])
    ])
  ]);
  devConfig.devtool = 'eval-source-map';
  return devConfig;
};

///////////// PRODUCTION CONFIG /////////////////
const baseProdCfg = BaseConfig({
  sass:{
    filename:'./styles/styles.[hash:8].css'
  },
  fileloader:{
    name:'[name].[hash:8].[ext]',    
    useRelativePath: true,
    outputPath: 'images/' 
  }
});
const getProductionConfig = () => {
  const outdir = path.resolve(__dirname, 'build','prod');
  const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
  return merge([{
      resolve:{
        //lazy-load nggak jalan kalo 
        //nggak pake ini
        extensions: ['.js', '.jsx']
      },
      externals: {
        'react':'React', //window.React
        'react-dom':'ReactDOM', //window.ReactDOM
        'react-router-dom':'ReactRouterDOM'
      }
    },
    {
      entry: ENTRY,
      output: {
        path:outdir,
        filename:'./js/bundle.[hash:8].js',
        chunkFilename:'./js/[name].chunk.[hash:8].js'
      }
    },
    baseProdCfg.modules(null,[
      {
        test: /\.(png|jpg|gif|svg)$/,
        use:{
          loader:'image-webpack-loader',
          options:{
            bypassOnDebug:true,
            mozjpeg: {
              progressive: true,
              quality: 65
            },
            // optipng.enabled: false will disable optipng
            optipng: {
              enabled: false,
            },
            pngquant: {
              quality: '65-90',
              speed: 4
            },
          }
        },
        enforce: 'pre'
      }
    ]),
    baseProdCfg.plugins([            
      new BundleAnalyzerPlugin({
        generateStatsFile:true,
        analyzerMode:'static',
        openAnalyzer: false
      }),            
      new OptimizeCSSAssetsPlugin(),
      new HtmlWebpackPlugin({
        title: 'PRODUCTION',
        template: './__webpack/__template.html',        
        filename:'index.html'
      }),
      new CleanPlugin([ outdir ])
    ])
  ]);
};

// webpack --env production|dev
module.exports = env => {
  if (env === 'production') {
    return getProductionConfig();
  }

  return getDevConfig();
};
