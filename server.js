const express = require('express');
const fs = require('fs');
const path = require('path');
const args = require('yargs').argv;

const buildDir = args.build || 'prod';

//HEROKU process.env
const port = process.env.PORT || args.port || 3000;
const dir = path.resolve(__dirname, 'build', buildDir);

const app = express();
app.use(express.static(dir));
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'build', buildDir, 'index.html'));
});

app.listen(port, () => {
  console.log('server is up at', port);
});
