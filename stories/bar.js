import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs/react';
import Bar from '../src/components/Bar';

const stories = storiesOf('Bar', module);

// Add the `withKnobs` decorator to add knobs support to your stories.
// You can also configure `withKnobs` as a global decorator.
stories.addDecorator(withKnobs);

stories
  .add('Render', () => <Bar />)
  .add('With text', () => <Bar text={text('Custom Text', 'My custom text')} />);
