import React from 'react';
import { storiesOf } from '@storybook/react';
import Header from '../src/components/Header';
import StoryRouter from 'storybook-router';

storiesOf('Header', module)
  .addDecorator(StoryRouter())
  .add('Render', () => <Header />);
