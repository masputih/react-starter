# ReactJS Starter Kit

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

[![code style: editorconfig](https://img.shields.io/badge/code_style-editorconfig-brightgreen.svg?style=flat-square)]
(http://editorconfig.org/)

[![lint: eslint](https://img.shields.io/badge/lint-eslint-red.svg?style=flat-square)]
(https://eslint.org/)

[![lint: stylelint](https://img.shields.io/badge/lint-stylelint-blue.svg?style=flat-square)]
(https://stylelint.io/)

Fitur:
- ESLint, Babel Stage 3, & Webpack
- Modular Webpack config (ide dari [SurviveJS: Webpack](https://survivejs.com/webpack/))
- SASS & StyleLint
- EditorConfig & Prettier
- Testing pake [Jest](https://facebook.github.io/jest/) & [Enzyme](http://airbnb.io/enzyme/)
- Optimasi file gambar: PNG, JPG, SVG, GIF. Manual pake Gulp atau otomatis pake webpack.
- Client-side Routing pake [HashRouter](https://reacttraining.com/react-router/web/api/HashRouter) dari [React-Router](https://reacttraining.com/react-router/)
- Express minimal server 
- Heroku-compatible
- Babel browser support: [Indonesia > 1%, IE9+, Edge](http://browserl.ist/?q=%3E1%25+in+ID%2C+ie+%3E%3D+9)
- Polyfill via [Polyfill.io](https://polyfill.io/v2/docs/)
- Dynamic Import ( Lazy-load )
- [Storybook](https://storybook.js.org) untuk functional test & dokumentasi/katalog komponen
- Build-output bisa diupload ke sebarang hosting ( s3 pake [aws-cli](https://aws.amazon.com/cli/), [surge](http://surge.sh/), heroku, dll). Contoh:
  - https://ab-react-starter.herokuapp.com/ (express)
  - https://ab-react-starter.surge.sh/ (statis)
  - http://ab-react-starter.s3-website-ap-southeast-1.amazonaws.com (statis)


## Bikin project dari template ini

```bash
$ git clone git@gitlab.com:masputih/react-starter.git myproject
$ cd myproject
# hapus direktori .git
$ rm -rf .git
# bikin repo baru
$ git init
```  


## Command

Cek browser support:

```bash
$ npx browserslist
and_chr 62
and_uc 11.4
android 4.4.3-4.4.4
android 4.4
chrome 63
chrome 62
edge 16
edge 15
edge 14
edge 13
edge 12
ie 11
ie 10
ie 9
ios_saf 11.0-11.2
op_mini all
samsung 4
```


### NPM Scripts

>Kalo pake NPM, ganti "yarn" dgn "npm run"

- Lint JS : `yarn lint:js`
- Lint SCSS : `yarn lint:scss`
- Testing pake Jest: `yarn test`
- Optimasi file gambar (manual): `gulp optimise:images`
- Build app versi dev (ada sourcemaps) : `yarn build:dev`
- Build app versi production : `yarn build:prod`
- Jalanin webpack dev server: `yarn start:dev`
- Jalanin server lokal buat testing production build: `yarn start`
- Jalanin storybook : `yarn storybook`

### DEPLOY 

#### Static hosting

- Jalanin `yarn build:prod`
- Upload semua file di `build/prod/` ke web server pake ftp, scp, surge, dll. 

#### Heroku

- bikin dulu app di Heroku: `heroku create <nama-app>`, mis. `heroku create myweb`. 
  Perintah ini otomatis bikin Git repo & nambahin entri `remote` di git config lokal.
- Push ke remote: `git push heroku master`
- Untuk deploy ke heroku dari non-master branch: 
    - Bikin branch: `git checkout -b experiment-branch`
    - Bikin app baru, beda dgn yg di master: `heroku create experiment-app` Nama app & branch nggak harus sama.
    - Tambahin entry git remote: `git remote add experiment-app <URL ke Git repo Heroku>`
    - Deploy: `git push experiment-app experiment-branch:master`
    - App bisa diakses di: `https://experiment-app.herokuapp.com`

