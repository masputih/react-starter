import { configure } from '@storybook/react';

function loadStories() {
  require('../stories/header.js');
  require('../stories/bar.js');
  // You can require as many stories as you need.
}

configure(loadStories, module);