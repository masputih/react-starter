import React from 'react';
import LazyloadedBar from '../components/LazyloadedBar.jsx';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shouldLoadBarComponent: false
    };

    this.doLoadBar = this.doLoadBar.bind(this);
  }

  doLoadBar() {
    this.setState({
      shouldLoadBarComponent: true
    });
    console.log('DO LOAD BAR');
    debugger;
  }

  render() {
    return (
      <div>
        Home <hr />
        <button onClick={this.doLoadBar}>Load Bar Component</button>
        {this.state.shouldLoadBarComponent ? <LazyloadedBar /> : ''}
      </div>
    );
  }
}
