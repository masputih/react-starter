import React from 'react';

const Bar = props => (
  <div>
    Lazy-loaded Bar Component
    {props.text ? <p>{props.text}</p> : ''}
  </div>
);

export default Bar;
