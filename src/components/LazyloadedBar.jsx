import React from 'react';
import Loadable from 'react-loadable';

const LazyloadedBar = Loadable({
  //nggak bisa pake ekstensi
  //jadi harus ada opsi resolve.extensions di webpack config
  loader: () => import('./Bar'),
  loading() {
    //loading message
    return <div>LOADING BAR COMPONENT</div>;
  }
});

export default LazyloadedBar;
