import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Header from './components/header/Header.jsx';
import Home from './pages/Home.jsx';
import About from './pages/About.jsx';

import './styles/global.scss';
import classes from './App.scss';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={classes.app}>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
        </Switch>
      </div>
    );
  }
}
