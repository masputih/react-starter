module.exports = {
  testPathIgnorePatterns:['/node_modules/','__tests/.*'],
  setupFiles:[
      'raf/polyfill', //requestAnimationFrame polyfill buat node
      '<rootDir>/__tests/setup-enzyme.js'
  ],
  snapshotSerializers:[
      'enzyme-to-json/serializer'
  ],
  'moduleNameMapper': {
    //biar css-modules ga bikin error di snapshot testing 
    '\\.(s?css)$': 'identity-obj-proxy'
  }
};