const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const pump = require('pump');
const debug = require('gulp-debug');
const awspublish = require('gulp-awspublish');

/**
 * Optimize src/images/
 */
gulp.task('optimise:images', () => {
  pump([
    gulp.src('./src/images/**/*'),
    imagemin(),
    gulp.dest('./src/images'),
    debug({ title: 'Optimise Image' })
  ]);
});

/**
https://www.npmjs.com/package/gulp-awspublish

*/
gulp.task('publish:aws', () => {
  const pub = awspublish.create({
    region: 'ap-southeast-1',
    params: {
      Bucket: 'ab-react-starter'
    }
  });
  const headers = {
    'Cache-Control': 'max-age:315360000, no-transform, public'
  };

  pump([
    gulp.src('./build/prod/**/*'),
    awspublish.gzip(),
    pub.publish(headers),
    pub.cache(),
    awspublish.reporter()
  ]);
});
